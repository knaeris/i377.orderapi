package dao;

import conf.DbConfig;
import dao.interfaces.IOrderDao;
import model.Order;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Tester {

    public static void main(String[] args) {
       ConfigurableApplicationContext ctx =
              new AnnotationConfigApplicationContext(DbConfig.class);



        IOrderDao dao = ctx.getBean(IOrderDao.class);
        Order order = new Order("till");

        dao.save(order);
        System.out.println(dao.findAll());
        ctx.close();
    }
}
