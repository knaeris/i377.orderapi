package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="orders")
@NoArgsConstructor
public class Order {

    public Order(@NotNull @Size(min = 2, max = 50) String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "order_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;
    @NotNull
    @Size(min=2, max=50)
    @Column(name="order_number")
    private String orderNumber;


    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(
            name = "order_rows",
            joinColumns=@JoinColumn(name = "orders_id",
                    referencedColumnName = "id")
    )
    @Valid
    private List<OrderRows> orderRows=new ArrayList<>();

}

