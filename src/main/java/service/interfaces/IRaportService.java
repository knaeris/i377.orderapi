package service.interfaces;

import model.Report;

public interface IRaportService {

    Report getReport();
}
