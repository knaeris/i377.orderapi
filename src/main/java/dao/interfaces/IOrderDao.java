package dao.interfaces;

import model.Order;
import model.OrderRows;
import model.Report;
import org.springframework.context.annotation.Bean;

import java.util.List;

public interface IOrderDao {

    void save(Order order);

    List<Order> findAll();

    void delete(Long id);

    Order findById(Long id);

    void deleteAll();


}
