package service;

import dao.interfaces.IOrderDao;
import model.Order;
import model.OrderRows;
import model.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.interfaces.IRaportService;

import java.util.ArrayList;
import java.util.List;

@Service
public class RaportService implements IRaportService {

    @Autowired
    IOrderDao dao;

    public Report getReport() {
        List<Order> orders = dao.findAll();
        List<OrderRows> orderRowList = new ArrayList<>();
        List<Long> turnoverWithoutVATList = new ArrayList<>();
        List<Long> sumList = new ArrayList<>();
        Report report = new Report();
        report.setCount((long) orders.size());

        for (Order order : orders) {
            order = (dao.findById(order.getId()));

            for (OrderRows orderRow : order.getOrderRows()) {
                orderRowList.add(orderRow);
                turnoverWithoutVATList.add(orderRow.getPrice()*orderRow.getQuantity());
                Long sum = orderRow.getPrice() *orderRow.getQuantity()/orders.size();
                sumList.add(sum);
            }
        }

        Long sum = sumList.stream().mapToLong(Long::longValue).sum();
        Long sumWithoutVAT = turnoverWithoutVATList.stream().mapToLong(Long::longValue).sum();
        report.setAverageOrderAmount(sum);
        report.setTurnoverWithoutVAT(sumWithoutVAT);
        Double VAT = 0.2;
        Double priceVAT = sumWithoutVAT*VAT;
        Long longVAT = Math.round(priceVAT);
        report.setTurnoverVAT(longVAT);
        report.setTurnoverWithVAT(longVAT+sumWithoutVAT);

        return report;
    }
}
