package dao;

import dao.interfaces.IOrderDao;
import model.Order;
import model.OrderRows;
import model.Report;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Primary
@Repository
public class OrderDao implements IOrderDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Order order) {
        System.out.println(order);
        if (order.getId() == null)
            em.persist(order);
        else
            em.merge(order);

    }

    @Override
    public List<Order> findAll() {
        return em.createQuery("select distinct o from Order o left join fetch o.orderRows",
                Order.class).getResultList();
    }

    @Transactional
    public void delete(Long id) {
        em.createQuery("delete from Order o where o.id=:id").setParameter("id", id).executeUpdate();

    }

    @Override
    public Order findById(Long id) {
        TypedQuery<Order> query = em.createQuery("select distinct o from Order o left join fetch o.orderRows where o.id=:id",
                Order.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }


    @Transactional
    public void deleteAll() {
        Query q1 = em.createQuery("delete from Order");

        q1.executeUpdate();

    }
}
