package model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OrderRows {

    @Column(name="item_name")
    private String itemName;
    @NotNull
    @Min(value = 1L)
    @Column(name="price")
    private Long price;
    @NotNull
    @Min(value = 1L)
    @Column(name="quantity")
    private Long quantity;

}

