package controller;

import dao.interfaces.IOrderDao;
import model.Order;
import model.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.RaportService;
import service.interfaces.IRaportService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private IOrderDao dao;
    @Autowired
    private IRaportService raportService;

    @GetMapping("orders")
    public List<Order> getOrders()
    {
        return dao.findAll();
    }

    @PostMapping("orders")
    public Order save(@RequestBody @Valid  Order order){

        dao.save(order);
        return getOrderById(order.getId());
    }

    @RequestMapping(method = RequestMethod.DELETE, path="orders")
    public void DeleteAllOrders(){
        dao.deleteAll();
    }
    @DeleteMapping("orders/{orderId}")
    public void deleteOrder(@PathVariable("orderId") Long orderId){
        dao.delete(orderId);
    }


    @GetMapping(path="orders/{orderId}")
    public Order getOrderById(@PathVariable("orderId")Long orderId){
        return dao.findById(orderId);
    }

    @GetMapping(path="orders/report")
    public Report getReport(){ return raportService.getReport();}

}