package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Report {
    private Long count;
    private Long averageOrderAmount;
    private Long turnoverWithoutVAT;
    private Long turnoverVAT;
    private Long turnoverWithVAT;

}
